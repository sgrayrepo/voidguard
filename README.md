## Summary
VoidGuard is a simple plugin to allow:

* Protecting the integrity of all "floor" and nether "ceiling" bedrock against Creative users and Tree growth glitching (currently only applies to Dark Oak trees)
* Removing the "random" bedrock generated in extra layers than layer 0 or (nether) layer 127
* Filling holes in bottom layer of floor bedrock

This plugin was created in response to a need to have the above features while being compatible with pre-existing worlds, new "vanilla" land generation, and custom terrain generators which don't automatically include these features. It is intentionally meant to be more minor utility than an epic piece of (software) art. :)

## Install
1. Add plugin jar into your plugins folder.
2. Stop and start your server again. Do not use "/reload" - I believe VoidGuard is safe for reload, but I can't speak for your other plugins!
3. Review at least the the default world setting (this applies to all worlds not specifically defined).
4. To have worlds with their own settings, make a copy of the "default" world setting in the config file with the world's name, and when set, use _/void reload_ or restart your server.

## Configuration
__timer__  
- section controlling the radius checks for the "flat" and "solid" options.  
__timer.radius__  
- approximate block distance to check from players, recommended/default value is 32  
__timer.interval__  
- rate at which the task runs, in approximate seconds  
__worlds__  
- list of per-world settings; do not remove "default" config entry, just set all its options to false if you don't want to use it.  
__worlds.default__  
- the settings that effect all worlds not individually defined  
__worlds.default.floor.flat__  
- if true, ignore layer 0 and replaces bedrock in layers 1-4 with either stone (overworld) or netherrack (nether)  
__worlds.default.floor.solid__  
- if true, any non-bedrock (including air) block in layer 0 is replaced by bedrock  
__worlds.default.floor.protected__  
- if true, prevents the breaking of bedrock at layer 0, both normal block breaking and tree glitch breaking  
__worlds.default.ceiling.flat__  
- (nether only) if true, ignore layer 127 and replaces bedrock in layers 123-126 with netherrack  
__worlds.default.ceiling.solid__  
- (nether only) if true, any non-bedrock (including air) block in layer 127 is replaced by bedrock  
__worlds.default.ceiling.protected__  
- (nether only) if true, prevents the breaking of bedrock at layer 127, both normal block breaking and tree glitch breaking
__hook-metrics__
- leave `true` to control metrics from PluginMetrics config, set `false` to prevent VoidGuard from interacting with metrics (even if `opt-out` is false)

Extra information about the configuration settings and how they're applied is on the [Detailed Information](http://dev.bukkit.org/bukkit-plugins/voidguard/pages/detailed-information/) page.

Default configuration file: [http://pastebin.com/T9CfjHxQ](http://pastebin.com/T9CfjHxQ)  

Example modified configuration file: [http://pastebin.com/tjb2nH1h](http://pastebin.com/tjb2nH1h)  

If you get console errors mentioning "snakeyaml" related to the VoidGuard config, paste the contents of your configuration file at a [YAML Parser/Validator](http://yaml-online-parser.appspot.com) and carefully read the errors to find the invalid formatting.

## Commands
To help keep VoidGuard smple (and lighter), I scrapped having a bunch of configuration commands.  
__/void reload__  
- freshly grabs configuration values from the config.yml

## Permissions
__voidguard.admin__  
- allows using the _/void reload_ command
__voidguard.override.protection__  
- allows normal breaking of both floor and ceiling bedrock

## Source / Dev Builds
The source for VoidGuard can be viewed at [https://bitbucket.org/sgray/voidguard/](https://bitbucket.org/sgray/voidguard/)

Additionally,  
Development builds of this project can be acquired at the provided continuous integration server.  
These builds have not been approved by the BukkitDev staff. Use them at your own risk.  
[https://sgray.ci.cloudbees.com/job/VoidGuard/](https://sgray.ci.cloudbees.com/job/VoidGuard/)

## Reporting Problems
Please explain errors/bugs with as much details as you can provide, instead of just saying "it doesn't work".  
Examples of information to include:

* OS version
* Java version (VoidGuard is compiled with _Java 7_)
* Server version ("Craftbukkit" or otherwise)
* VoidGuard version
* VoidGuard configuration file (use Pastebin or similar service please!)
* Steps taken to repeat problem

Also, please don't report "problems" you have not found! If Craftbukkit updates to a new Minecraft version, actually look to see if VoidGuard works before assuming it does not.

## Metrics
Hidendra's Metrics have been added to the project, because statistics are fun (and it make it easier to calculate plugin usefulness).
Disable metrics from sending for any plugin by going into _(your plugin folder)/PluginMetrics/config.yml_ and setting `opt-out: true`
Disable metrics just for VoidGuard by setting `hook-metrics: false` in VoidGuard's config.

## More Info
Please make sure to read the [Detailed Information](http://dev.bukkit.org/bukkit-plugins/voidguard/pages/detailed-information/) page if you're still a little lost. :)