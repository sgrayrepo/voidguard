package me.sgray.plugin.voidguard;

import java.util.LinkedList;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.TreeType;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.world.StructureGrowEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

public class VoidGuard extends JavaPlugin implements Listener {
    ConfigReader config = new ConfigReader(this);
    DFPPlayerTimer DFPTimer = new DFPPlayerTimer(this);
    List<String> playerCooldown = new LinkedList<String>();
    int worldTask;

    public void onDisable() {
        endWorldTimer();
    }

    public void onEnable() {
        getServer().getPluginManager().registerEvents(this, this);
        getCommand("voidguard").setExecutor(new CommandListener(this));
        getConfig().options().copyDefaults(true);
        saveConfig();
        resetWorldTimer();
    }

    @EventHandler(ignoreCancelled=false)
    public void onBlockBreak(BlockBreakEvent event) {
        if (!(event.getPlayer().hasPermission("voidguard.override.protection"))
              && !(event.getPlayer().hasPermission("voidguard.override.preventbreak"))) {
            Block subjBlock = event.getBlock();
            Location subjLoc = subjBlock.getLocation();
            String worldName = subjLoc.getWorld().getName();
            if(subjBlock.getType() == Material.BEDROCK && config.shouldPreventBreak(worldName)) {
                if (config.getToBedrockLayers(worldName).contains((int) subjLoc.getY())) {
                    event.setCancelled(true);
                }
            }
        }
    }

    @EventHandler(ignoreCancelled=false)
    public void onTreeGrow(StructureGrowEvent event) {
        Location subjLoc = event.getLocation();
        if(event.getSpecies().equals(TreeType.DARK_OAK) && (subjLoc.getY() < 5 || subjLoc.getY() > 122)) {
            List<BlockState> tBlocks = event.getBlocks();
            for(BlockState bState : tBlocks) {
                Location blockLoc = bState.getLocation();
                if(bState.getType().equals(Material.LOG_2)) {
                    if (config.getToBedrockLayers(blockLoc.getWorld().getName()).contains((int) blockLoc.getY())) {
                        bState.setType(Material.BEDROCK);
                        bState.update();
                    }
                }
            }
        }
    }

    @EventHandler(ignoreCancelled = false, priority = EventPriority.MONITOR)
    public void onPlayerMove(PlayerMoveEvent event) {
        String wN = event.getPlayer().getWorld().getName();
        if (!config.useTrespassCommands(wN)
                || (event.getTo().getY() <= config.getTrespassUpper(wN)
                && event.getTo().getY() >= config.getTrespassLower(wN))) {
            return;
        } else {
            if (!event.getPlayer().hasPermission("voidguard.override.trespass")
                    && Math.abs(event.getFrom().getY() - event.getTo().getY()) > 0) {
                if (playerCooldown == null || playerCooldown.contains(event.getPlayer().getName())) {
                    return;
                }
                String edge = (event.getTo().getY() >= config.getTrespassUpper(wN) ? "above" : "below");
                List<String> commands = config.getTrespassCommands(event.getPlayer().getWorld().getName());
                runTrespassCommands(event.getPlayer(), commands, edge);
            }
        }
    }

    protected void endWorldTimer() {
        getServer().getScheduler().cancelTask(worldTask);
        worldTask = 0;
    }

    protected int startWorldTimer() {
        worldTask = getServer().getScheduler().runTaskTimer(this, DFPTimer, 100L, config.getTimerInterval()).getTaskId();
        return worldTask;
    }

    protected void resetWorldTimer() {
        if (worldTask != 0) {
            endWorldTimer();
        } 
        if (config.shouldStartWorldTimer()) {
            this.startWorldTimer();
        }
    }

    protected void runTrespassCommands(Player player, List<String> commands, String edge) {
        String playerName = player.getName();
        for (String cmd : commands) {
            if ((!cmd.startsWith("below:") && !cmd.startsWith("above:")) 
                    || (cmd.startsWith("below:") && edge.equals("below")) || (cmd.startsWith("above:") && edge.equals("above"))) {
                if (cmd.startsWith("above:")) {
                    cmd = cmd.replaceFirst("above\\:", "");
                }
                if (cmd.startsWith("below:")) {
                    cmd = cmd.replaceFirst("below\\:", "");
                }
                cmd = cmd.replaceAll("<player>", playerName);
                cmd = cmd.replaceAll("<world>", player.getWorld().getName());
                if (cmd.startsWith("p:")) {
                    cmd = cmd.replaceFirst("p\\:", "");
                    player.performCommand(cmd);
                } else {
                    getServer().dispatchCommand(getServer().getConsoleSender(), cmd);
                }
            }
        }
        playerCooldown.add(playerName);
        getServer().getScheduler().runTaskLaterAsynchronously(this, new BukkitRunnable() {
            public void run() {
                if (!playerCooldown.isEmpty()) {
                    playerCooldown.remove(0);
                }
            }
        }, 60L);
    }

    protected void doReload(CommandSender sender) {
        reloadConfig();
        resetWorldTimer();
        sender.sendMessage("VoidGuard config reloaded.");
        if(!isConsole(sender)) {
            getLogger().info("The VoidGuard config has been reloaded.");
        }
    }

    protected boolean isConsole(CommandSender sender) {
        return !(sender instanceof Player) ? true : false;
    }

    protected boolean isInteger(String value) {
        try {
            Integer.parseInt(value);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
