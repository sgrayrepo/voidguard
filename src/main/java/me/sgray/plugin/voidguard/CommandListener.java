package me.sgray.plugin.voidguard;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandListener implements CommandExecutor {
    private final VoidGuard plugin;
    DFPModifier RadiusModifier = new DFPModifier();

    public CommandListener(VoidGuard plugin) {
        this.plugin = plugin;
    }

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        String cmdName = cmd.getName().toLowerCase();
        if(cmdName.equals("voidguard")) {
            if(sender.hasPermission("voidguard.admin")) {
                switch(args.length) {
                    case 0:
                        sender.sendMessage("Reload the config with /void reload");
                        break;
                    case 1:
                        if(args[0].equals("reload")) {
                          plugin.doReload(sender);
                        } else if (!plugin.isConsole(sender) && (args[0].equals("setbedrock") || args[0].equals("sbr"))) {
                          Player player = (Player) sender;
                          RadiusModifier.modifyBedrockToAny(player.getLocation(), plugin.config.getBedrockCommandDistance(), 
                              plugin.config.getFromBedrockLayers(player.getWorld().getName()), 
                              plugin.config.getReplaceMaterial(player.getWorld().getName()));
                          RadiusModifier.modifyToBedrock(player.getLocation(), plugin.config.getBedrockCommandDistance(), 
                              plugin.config.getToBedrockLayers(player.getWorld().getName()));
                        } else {
                          msgNotValid();
                        }
                        break;
                    case 2:
                        if (!plugin.isConsole(sender) && (args[0].equals("setbedrock") || args[0].equals("sbr"))) {
                            if (plugin.isInteger(args[1])) {
                                Player player = (Player) sender;
                                RadiusModifier.modifyBedrockToAny(player.getLocation(), Math.abs(Integer.parseInt(args[1])), 
                                        plugin.config.getFromBedrockLayers(player.getWorld().getName()), 
                                        plugin.config.getReplaceMaterial(player.getWorld().getName()));
                                RadiusModifier.modifyToBedrock(player.getLocation(), Math.abs(Integer.parseInt(args[1])), 
                                        plugin.config.getToBedrockLayers(player.getWorld().getName()));
                            } else {
                                msgNotValid();
                            }
                        } else {
                            msgNotValid();
                        }
                        break;
                    default:
                        msgNotValid();
                        break;
                }
            } else {
                sender.sendMessage(msgNoPermission());
            }
            return true;
        }
        return false;
    }

    private String msgNoPermission() {
        return "You do not have permission for this command.";
    }

    private String msgNotValid() {
        return "That is not a valid command for VoidGuard, please check your command syntax.";
    }
}
