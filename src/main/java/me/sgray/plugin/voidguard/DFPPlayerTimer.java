package me.sgray.plugin.voidguard;

import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class DFPPlayerTimer extends BukkitRunnable {
    private final VoidGuard plugin;
    DFPModifier RadiusModifier = new DFPModifier();

    public DFPPlayerTimer(VoidGuard plugin) {
        this.plugin = plugin;
    }

    public void run() {
        Player[] people = plugin.getServer().getOnlinePlayers();
        if (!(people.length == 0)) {
            for(Player p : people) {
                World world = p.getWorld();
                RadiusModifier.modifyBedrockToAny(p.getLocation(), 16, 
                        plugin.config.getFromBedrockLayers(world.getName()), plugin.config.getReplaceMaterial(world.getName()));
                RadiusModifier.modifyToBedrock(p.getLocation(), 16, plugin.config.getToBedrockLayers(world.getName()));
            }
        }
    }
}
