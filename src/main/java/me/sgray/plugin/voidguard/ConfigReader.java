package me.sgray.plugin.voidguard;

import java.util.LinkedList;
import java.util.List;

import org.bukkit.Material;

public class ConfigReader {
    private final VoidGuard plugin;

    public ConfigReader(VoidGuard plugin) {
        this.plugin = plugin;
    }

    protected boolean shouldPreventBreak(String world) {
        return plugin.getConfig().getBoolean("worlds." + configUseWorld(world) + ".prevent-bedrock-layer-break");
    }

    protected LinkedList<Integer> getToBedrockLayers(String world) {
        List<Integer> rawLayers = plugin.getConfig().getIntegerList("worlds." + configUseWorld(world) + ".set-as-bedrock-layers");
        LinkedList<Integer> bedrockLayers = new LinkedList<Integer>();
        for (Integer layer : rawLayers) {
            bedrockLayers.add(layer);
        }
        return bedrockLayers;
    }

    protected LinkedList<Integer> getFromBedrockLayers(String world) {
        List<Integer> rawLayers = plugin.getConfig().getIntegerList("worlds." + configUseWorld(world) + ".remove-bedrock-layers");
        LinkedList<Integer> clearLayers = new LinkedList<Integer>();
        for (Integer layer : rawLayers) {
            clearLayers.add(layer);
        }
        return clearLayers;
    }

    protected Material getReplaceMaterial(String world) {
        return Material.matchMaterial(plugin.getConfig().getString("worlds." + configUseWorld(world) + ".replacement-material"));
    }

    protected int getBedrockCommandDistance() {
        return Math.abs(plugin.getConfig().getInt("bedrock-command-distance", 32));
    }

    protected long getTimerInterval() {
        return (plugin.getConfig().getBoolean("slower-auto-changes") ? 1200L : 400L );
    }

    protected boolean hookMetrics() {
        return plugin.getConfig().getBoolean("hook-metrics");
    }

    protected boolean useTrespassCommands(String world) {
        return plugin.getConfig().getBoolean("worlds." + configUseWorld(world) + ".use-trespass-commands");
    }

    protected int getTrespassUpper(String world) {
        return Math.abs(plugin.getConfig().getInt("worlds." + configUseWorld(world) + ".trespassing-above-layer"));
    }

    protected int getTrespassLower(String world) {
        return plugin.getConfig().getInt("worlds." + configUseWorld(world) + ".trespassing-below-layer");
    }

    protected List<String> getTrespassCommands(String world) {
        return plugin.getConfig().getStringList("worlds." + configUseWorld(world) + ".trespass-commands");
    }

    protected boolean shouldStartWorldTimer() {
        return (plugin.getConfig().getBoolean("auto-bedrock-changes"));
    }

    protected String configUseWorld(String world) {
        return (plugin.getConfig().getConfigurationSection("worlds." + world) != null) ? world : "default";
    }
}
