package me.sgray.plugin.voidguard;

import java.util.LinkedList;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.Material;

/**
 * This class represents methods that modify a game world 
 * based on their distance from a particular coordinate 
 * location.
 */

public class DFPModifier {
    protected void modifyBedrockToAny(Location pLoc, int distance, List<Integer> layers, Material matTo) {
        for (Location loc : locationsToModify(pLoc, distance, layers)) {
            if (loc.getBlock().getType().equals(Material.BEDROCK)) {
                setMaterial(loc, Material.BEDROCK, matTo);
            }
        }
    }

    protected void modifyToBedrock(Location pLoc, int distance, List<Integer> layers) {
        for (Location loc : locationsToModify(pLoc, distance, layers)) {
            Material blockMat = loc.getBlock().getType();
            if (!blockMat.equals(Material.BEDROCK)) {
                setMaterial(loc, blockMat, Material.BEDROCK);
            }
        }
    }

    protected LinkedList<Location> locationsToModify(Location center, int distance, List<Integer> layers) {
        LinkedList<Location> netLoc = new LinkedList<Location>();
        int pX = center.getBlockX();
        int pZ = center.getBlockZ();
        for(int i = pX - distance; i < pX + distance; i++) {
            for(int j = pZ - distance; j < pZ + distance; j++) {
                for (int layer : layers) {
                    netLoc.add(new Location(center.getWorld(), i, layer, j));
                }
            }
        }
        return netLoc;
    }

    private void setMaterial(Location loc, Material matFrom, Material matTo) {
        if (loc != null && matFrom != null && matTo != null) {
            loc.getBlock().setType(matTo);
        }
    }
}
